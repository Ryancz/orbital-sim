#ifndef DATA_FILE_H
#define DATA_FILE_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>

class dataFile{

    bool open_status;
    std::string filename;
    std::string filetype;
    std::string throwaway;
//del    uint ICRF2_lines = 11;
    int in_int;
    double in_double;

    public:

    std::vector <int> id_vec;
    std::vector < std::vector <double> > data_vec;

    //private:

    void process(){
        if(filetype == "ICRF2"){
            std::ifstream infile(filename);
            open_status = infile.is_open();
            if(open_status){
                std::string str;
                std::vector <double> in_vec (7, std::nan(""));
                std::getline(infile, str); // header line
                while (std::getline(infile, str)) {
                    id_vec.push_back(std::stoi(str.substr(0,28)));
                    for(int i=1; i<8; i++){
                        in_vec[i-1] = (std::stod(str.substr(i*28,28)));
                    }
                    data_vec.push_back(in_vec);                  
                }
            }
        }else if(filetype == "JPL"){

        }else{
            open_status = false;
        }


    }

    public:

    dataFile(){
        open_status = false;
    }


    dataFile(std::string init_filename, std::string init_filetype){
        filename = init_filename;
        filetype = init_filetype;
        process();
    }

    bool exists(){
        return open_status;
    }

};


#endif