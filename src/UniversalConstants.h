#ifndef UNIVERSAL_CONSTANTS_H
#define UNIVERSAL_CONSTANTS_H

#include "SigFigMath.h"

sigFig G(6.67259E-11, 6);
// G source : JPL (https://ssd.jpl.nasa.gov/?constants)
sigFig AU(149597870700, 12);
// AU source : JPL (https://ipnpr.jpl.nasa.gov/progress_report/42-196/196C.pdf)
sigFig Day(86400,5);
// 3600 s * 24 hours

#endif