#ifndef SOI_H
#define SOI_H

#include "CelestialBody.h"
#include "Craft.h"
#include <vector>
#include <cmath>
 
bool in_SOI(craft vehicle, CelestialBody body, CelestialBody parent_body){
    double semi_major_axis = -1.0 / ((vec_3d_addSub(body.vel(),parent_body.vel(),-1).mag_sq() /  parent_body.mu()) - (2.0 / vec_3d_addSub(body.pos(),parent_body.pos(),-1).mag()));
    double SOI_radius = semi_major_axis*pow((body.mu() / parent_body.mu()),(2.0/5.0));
    if(vec_3d_addSub(vehicle.pos,body.pos(),-1).mag() < SOI_radius){
        return true;
    }else{
        return false;
    }
}

int get_parent(){

    return 0;
}

int SOI(craft vehicle, std::vector <CelestialBody> bodies){
    int ID;
    int min_id = 10000;
    for(uint i=0; i<bodies.size(); i++){
        min_id = std::min(min_id, bodies[i].id());
    }
    int id_mag;
    // find the magnitude of the id, then look at the "parent" id (same first number, one less magnitude)

    //

    return ID;
}


#endif