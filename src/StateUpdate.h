#ifndef STATE_UPDATE_H
#define STATE_UPDATE_H

#include "Craft.h"
#include "SimData.h"

void stateUpdate(craft & vehicle, simData sim){
    // updates the velocity and position state of the input craft

    vehicle.vel.x = vehicle.vel.x + (vehicle.acc.x * sim.timestep());
    vehicle.vel.y = vehicle.vel.y + (vehicle.acc.y * sim.timestep());
    vehicle.vel.z = vehicle.vel.z + (vehicle.acc.z * sim.timestep());

    vehicle.pos.x = vehicle.pos.x + (vehicle.vel.x * sim.timestep());
    vehicle.pos.y = vehicle.pos.y + (vehicle.vel.y * sim.timestep());
    vehicle.pos.z = vehicle.pos.z + (vehicle.vel.z * sim.timestep());

}


#endif