#ifndef SIM_DATA_H
#define SIM_DATA_H

class simData{

    double sim_MET;
    double sim_timestep = 0.001;

    public:

    simData(){
        sim_MET = 0.0;
    }



    double MET(){
        return sim_MET;
    }
    double timestep(){
        return sim_timestep;
    }
};


#endif