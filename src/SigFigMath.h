#ifndef SIG_FIG_MATH_H
#define SIG_FIG_MATH_H

#include <cmath>

#include <iostream>

// sigFig rounding
double sigFig_round(double value, int prec){
    int i = INT16_MIN;
    do{
        i++;
    }while(value / pow(10.0,static_cast<double>(i)) > 1.0);
    value = value / pow(10.0,static_cast<double>(i));
    value = round(value * pow(10.0,static_cast<double>(prec))) * pow(10.0,static_cast<double>(i-prec));
    return value;
}

// object which contains the value and precision of said value

class sigFig{
    double sigFig_value;
    int    sigFig_prec;

    public:
    sigFig(){
        sigFig_value = 0.0;
        sigFig_prec  = 0;
    }
    sigFig(double set_value, int set_prec){
        set_value = sigFig_round(set_value, set_prec);
        sigFig_value = set_value;
        sigFig_prec  = set_prec;
    }

    void set(double set_value, int set_prec){
        sigFig_value = set_value;
        sigFig_prec  = set_prec;
    }

    double value(){
        return sigFig_value;
    }
    double prec(){
        return sigFig_prec;
    }

};


sigFig sigFig_multiply(sigFig a, sigFig b){
    int min_prec = std::min(a.prec(),b.prec());
    double value = (a.value() * b.value());
    value = sigFig_round(value,min_prec);
    return sigFig(value,min_prec);
}


#endif