#ifndef GRAVITY_H
#define GRAVITY_H

#include "VecMath.h"
#include "Craft.h"
#include "CelestialBody.h"

vec_3d gravity(craft vehicle, CelestialBody body){
    double grav_scale = -1.0 * body.mu() / pow(vehicle.pos.mag(),3.0);
    vec_3d result(vehicle.pos.x * grav_scale, vehicle.pos.y * grav_scale, vehicle.pos.z * grav_scale);
    return result;
}

#endif