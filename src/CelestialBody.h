#ifndef CELESTIAL_BODY_H
#define CELESTIAL_BODY_H

#include <string>
#include <fstream>
#include <cmath>
#include "UniversalConstants.h"
#include "SigFigMath.h"
#include "DataFile.h"
#include "VecMath.h"

class CelestialBody {

    // health data
    bool body_init_successful;

    // base data
    int body_id;
    // intrinsic body values
    double body_mu;                 // m^3 / s^2

    // current body values
    vec_3d body_pos;
    vec_3d body_vel;


    public:

    CelestialBody(){
        body_mu = std::nan("");
        body_pos.x = std::nan("");
        body_pos.y = std::nan("");
        body_pos.z = std::nan("");
        body_vel.x = std::nan("");
        body_vel.y = std::nan("");
        body_vel.z = std::nan("");
        body_id = -1;
        body_init_successful     = false;
    }
    CelestialBody(dataFile bodyfile, int ICRF_i){
        try{
            body_mu = bodyfile.data_vec[ICRF_i][0]*1E9;
            body_pos.x = bodyfile.data_vec[ICRF_i][1] * AU.value(); // from AU to m
            body_pos.y = bodyfile.data_vec[ICRF_i][2] * AU.value();
            body_pos.z = bodyfile.data_vec[ICRF_i][3] * AU.value();
            body_vel.x = bodyfile.data_vec[ICRF_i][4] * AU.value() / Day.value(); // from AU/day to m/s
            body_vel.y = bodyfile.data_vec[ICRF_i][5] * AU.value() / Day.value();
            body_vel.z = bodyfile.data_vec[ICRF_i][6] * AU.value() / Day.value();
            body_id = bodyfile.id_vec[ICRF_i];
        }
        catch (...){
            CelestialBody();
            // maybe also send a line to a log file
        }
    }

    // health data
    bool init_successful(){
        return body_init_successful;
    }

    // base_data
    int id(){
        return body_id;
    }

    // intrinsic body values
    double mu(){
        return body_mu;
    }

    // current body values
    vec_3d pos(){
        return body_pos;
    }
    vec_3d vel(){
        return body_vel;
    }

};

#endif
