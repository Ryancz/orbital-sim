#ifndef VEC_MATH_H
#define VEC_MATH_H

#include <cmath>

class vec_3d{

    public:

    double x;
    double y;
    double z;

    double mag(){
        return sqrt((x*x) + (y*y) + (z*z));
    }
    double mag_sq(){
        return ((x*x) + (y*y) + (z*z));
    }

    vec_3d(){
        x = std::nan("");
        y = std::nan("");
        z = std::nan("");
    }
    vec_3d(double in_x, double in_y, double in_z){
        x = in_x;
        y = in_y;
        z = in_z;
    }

};

double vec_3d_dot(vec_3d a, vec_3d b){
    return (a.x*b.x + a.y*b.y + a.z*b.z);
}
vec_3d vec_3d_cross(vec_3d a, vec_3d b){
    vec_3d result;
    result.x = a.y*b.z - a.z*b.y;
    result.y = a.z*b.x - a.x*b.z;
    result.z = a.x*b.y - a.y*b.x;
    return result;
}
vec_3d vec_3d_addSub(vec_3d a, vec_3d b, double sign){
    vec_3d result;
    result.x = a.x + (sign*b.x);
    result.y = a.y + (sign*b.y);
    result.z = a.z + (sign*b.z);
    return result;
}


#endif