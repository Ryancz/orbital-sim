#ifndef CRAFT_H
#define CRAFT_H

#include "VecMath.h"

class craft{

    bool craft_init_successful;

    public:

    vec_3d pos;
    vec_3d vel;
    vec_3d acc;

    craft(){
        craft_init_successful = false;
    }

    bool init_successful(){
        return craft_init_successful;
    }

};




#endif