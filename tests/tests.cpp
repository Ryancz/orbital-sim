#include <gtest/gtest.h>
#include <cmath>
#include <string>
#include <vector>

#include "CelestialBody.h"
#include "UniversalConstants.h"
#include "SigFigMath.h"
#include "Craft.h"
#include "VecMath.h"
#include "SimData.h"
#include "StateUpdate.h"
#include "Gravity.h"
#include "DataFile.h"
#include "SOI.h"


std::string data_file_loc = "./../data/ICRF2_Data";


TEST(SphereOfInfluenceTests, in_Earth_SOI){
    dataFile bodyDataFile = dataFile(data_file_loc,"ICRF2");
    CelestialBody Sun = CelestialBody(bodyDataFile, 0);
    CelestialBody Earth = CelestialBody(bodyDataFile, 3);
    craft test_craft = craft();
    std::vector <CelestialBody> body_list (2);
    body_list[0] = Sun;
    body_list[1] = Earth;
    test_craft.pos.x = Earth.pos().x + 9246489423.0;
    test_craft.pos.y = Earth.pos().y;
    test_craft.pos.z = Earth.pos().z;
    EXPECT_EQ(3, SOI(test_craft, body_list));
}
TEST(SphereOfInfluenceTests, in_Sun_SOI){
    dataFile bodyDataFile = dataFile(data_file_loc,"ICRF2");
    CelestialBody Sun = CelestialBody(bodyDataFile, 0);
    CelestialBody Earth = CelestialBody(bodyDataFile, 3);
    craft test_craft = craft();
    std::vector <CelestialBody> body_list (2);
    body_list[0] = Sun;
    body_list[1] = Earth;
    test_craft.pos.x = Earth.pos().x + 924648943.99;
    test_craft.pos.y = Earth.pos().y;
    test_craft.pos.z = Earth.pos().z;
    EXPECT_EQ(0, SOI(test_craft, body_list));
}
TEST(SphereOfInfluenceTests, body_parent_moon){
    
}
TEST(SphereOfInfluenceTests, body_parent_planet){

}
TEST(SphereOfInfluenceTests, body_parent_sun){

}
TEST(SphereOfInfluenceTests, in_SOI){
    dataFile bodyDataFile = dataFile(data_file_loc,"ICRF2");
    CelestialBody Sun = CelestialBody(bodyDataFile, 0);
    CelestialBody Earth = CelestialBody(bodyDataFile, 3);
    craft test_craft = craft();
    test_craft.pos.x = Earth.pos().x + 924648943.00;
    test_craft.pos.y = Earth.pos().y;
    test_craft.pos.z = Earth.pos().z;
    EXPECT_TRUE(in_SOI(test_craft, Earth, Sun));
}
TEST(SphereOfInfluenceTests, not_in_SOI){
    dataFile bodyDataFile = dataFile(data_file_loc,"ICRF2");
    CelestialBody Sun = CelestialBody(bodyDataFile, 0);
    CelestialBody Earth = CelestialBody(bodyDataFile, 3);
    craft test_craft = craft();
    test_craft.pos.x = Earth.pos().x + 924648943.99;
    test_craft.pos.y = Earth.pos().y;
    test_craft.pos.z = Earth.pos().z;
    EXPECT_FALSE(in_SOI(test_craft, Earth, Sun));
}
TEST(vecMathTests, magnitude_squared){
    vec_3d a = vec_3d(0.0, -98.5, 12.98);
    vec_3d b = vec_3d(999.78, -55.2, -0.0);
    vec_3d c = vec_3d(3.0, 4.0, 12.0);
    EXPECT_DOUBLE_EQ(9.8707304E3, a.mag_sq());
    EXPECT_DOUBLE_EQ(1002607.0884, b.mag_sq());
    EXPECT_DOUBLE_EQ(169.0, c.mag_sq());
}
TEST(vecMathTests, add_subtract){
    vec_3d a = vec_3d(15.8, 97.5, 999.99);
    vec_3d b = vec_3d(-1.5, 5.8, -9876.54);

    ASSERT_DOUBLE_EQ(14.3, vec_3d_addSub(a,b,1).x);
    ASSERT_DOUBLE_EQ(103.3, vec_3d_addSub(a,b,1).y);
    ASSERT_DOUBLE_EQ(-8876.55, vec_3d_addSub(a,b,1).z);
    ASSERT_DOUBLE_EQ(vec_3d_addSub(b,a,1).x, vec_3d_addSub(a,b,1).x);
    ASSERT_DOUBLE_EQ(vec_3d_addSub(b,a,1).y, vec_3d_addSub(a,b,1).y);
    ASSERT_DOUBLE_EQ(vec_3d_addSub(b,a,1).z, vec_3d_addSub(a,b,1).z);
    ASSERT_DOUBLE_EQ(17.3, vec_3d_addSub(a,b,-1).x);
    ASSERT_DOUBLE_EQ(-17.3, vec_3d_addSub(b,a,-1).x);
}
TEST(CelestialBodyTests, id){
    dataFile bodyDataFile = dataFile(data_file_loc,"ICRF2");
    CelestialBody test_body = CelestialBody(bodyDataFile, 3);
    EXPECT_EQ(3,test_body.id());
}
TEST(CelestialBodyTests, init_velocity){
    dataFile bodyDataFile = dataFile(data_file_loc,"ICRF2");
    CelestialBody test_body = CelestialBody(bodyDataFile, 3);
    EXPECT_DOUBLE_EQ(+0.01681126830978379448*149597870700/86400, test_body.vel().x);
    EXPECT_DOUBLE_EQ(+0.00174830923073434441*149597870700/86400, test_body.vel().y);
    EXPECT_DOUBLE_EQ(+0.00075820289738312913*149597870700/86400, test_body.vel().z);
}
TEST(CelestialBodyTests, init_position){
    dataFile bodyDataFile = dataFile(data_file_loc,"ICRF2");
    CelestialBody test_body = CelestialBody(bodyDataFile, 3);
    EXPECT_DOUBLE_EQ(+0.12051741410138465477*149597870700, test_body.pos().x);
    EXPECT_DOUBLE_EQ(-0.92583847476914859295*149597870700, test_body.pos().y);
    EXPECT_DOUBLE_EQ(-0.40154022645315222236*149597870700, test_body.pos().z);
}
TEST(gravitationalTests, acceleration){
    dataFile bodyDataFile = dataFile(data_file_loc,"ICRF2");
    CelestialBody test_body = CelestialBody(bodyDataFile, 3);
    craft test_craft = craft();
    test_craft.pos.x = 6378136.6;
    test_craft.pos.y = -150700.0;
    test_craft.pos.z = 98.78;
    vec_3d grav_acc = gravity(test_craft, test_body);

    // should fail now, new mu value
    EXPECT_DOUBLE_EQ(-9.790087238414555,grav_acc.x);
    EXPECT_DOUBLE_EQ(0.2313161726309019,grav_acc.y);
    EXPECT_DOUBLE_EQ(-1.516218416222992E-4,grav_acc.z);
}
TEST(vecMathTests, magnitude){
    vec_3d a = vec_3d(0.0, -98.5, 12.98);
    vec_3d b = vec_3d(999.78, -55.2, -0.0);
    vec_3d c = vec_3d(3.0, 4.0, 12.0);
    EXPECT_DOUBLE_EQ(99.351549560135197, a.mag());
    EXPECT_DOUBLE_EQ(1001.302695691967, b.mag());
    EXPECT_DOUBLE_EQ(13.0, c.mag());
}
TEST(state_update, updates_position){
    craft test_craft = craft();
    test_craft.pos.x = 0.0;
    test_craft.pos.y = 20.0;
    test_craft.pos.z = 100.0;
    test_craft.vel.x = 0.0;
    test_craft.vel.y = -0.0;
    test_craft.vel.z = 1.0;
    test_craft.acc.x = 0.1;
    test_craft.acc.y = -9.81;
    test_craft.acc.z = -200.0;
    simData sim = simData();
    stateUpdate(test_craft, sim);
    EXPECT_DOUBLE_EQ(0.0000001, test_craft.pos.x);
    EXPECT_DOUBLE_EQ(19.99999019, test_craft.pos.y);
    EXPECT_DOUBLE_EQ(100.0008, test_craft.pos.z);
    EXPECT_DOUBLE_EQ(0.0001, test_craft.vel.x);
    EXPECT_DOUBLE_EQ(-0.00981, test_craft.vel.y);
    EXPECT_DOUBLE_EQ(0.8, test_craft.vel.z);
}
TEST(SimDataTests, MET_and_timestep){
    simData sim = simData();
    EXPECT_DOUBLE_EQ(0.0, sim.MET());
    EXPECT_DOUBLE_EQ(0.001, sim.timestep());
}
TEST(craftTests, craft_acceleration_on_empty_init){
    craft test_craft = craft();
    EXPECT_TRUE(std::isnan(test_craft.acc.x));
    EXPECT_TRUE(std::isnan(test_craft.acc.y));
    EXPECT_TRUE(std::isnan(test_craft.acc.z));
}
TEST(craftTests, craft_velocity_on_empty_init){
    craft test_craft = craft();
    EXPECT_TRUE(std::isnan(test_craft.vel.x));
    EXPECT_TRUE(std::isnan(test_craft.vel.y));
    EXPECT_TRUE(std::isnan(test_craft.vel.z));
}
TEST(craftTests, craft_position_on_empty_init){
    craft test_craft = craft();
    EXPECT_TRUE(std::isnan(test_craft.pos.x));
    EXPECT_TRUE(std::isnan(test_craft.pos.y));
    EXPECT_TRUE(std::isnan(test_craft.pos.z));
}
TEST(vecMathTests, cross_product){
    vec_3d a = vec_3d(15.8, 97.5, 999.99);
    vec_3d b = vec_3d(-1.5, 5.8, -9876.54);
    vec_3d c = vec_3d(-67.8, -2.0, -0.0);

    ASSERT_DOUBLE_EQ(-968762.592, vec_3d_cross(a,b).x);
    ASSERT_DOUBLE_EQ(154549.347, vec_3d_cross(a,b).y);
    ASSERT_DOUBLE_EQ(237.89, vec_3d_cross(a,b).z);

    ASSERT_DOUBLE_EQ(1999.98, vec_3d_cross(a,c).x);
    ASSERT_DOUBLE_EQ(-67799.322, vec_3d_cross(a,c).y);
    ASSERT_DOUBLE_EQ(6578.9, vec_3d_cross(a,c).z);
}
TEST(vecMathTests, dot_product){
    vec_3d a = vec_3d(15.8, 97.5, 999.99);
    vec_3d b = vec_3d(-1.5, 5.8, -9876.54);
    vec_3d c = vec_3d(-67.8, -2.0, -0.0);

    ASSERT_DOUBLE_EQ(-9875899.4346, vec_3d_dot(a,b));
    ASSERT_DOUBLE_EQ(90.1, vec_3d_dot(c,b));
    ASSERT_DOUBLE_EQ(-1266.24, vec_3d_dot(a,c));
}
TEST(vecMathTests, init_tests){
    vec_3d test_vec = vec_3d(10.0, 3.14, 9999999.99);
    ASSERT_DOUBLE_EQ(10.0, test_vec.x);
    ASSERT_DOUBLE_EQ(3.14, test_vec.y);
    ASSERT_DOUBLE_EQ(9999999.99, test_vec.z);
}
TEST(craftTests, craft_initialization){
    craft test_craft = craft();
    ASSERT_FALSE( test_craft.init_successful() );
}
TEST(sigFigMath, sigFig_multiply){
    sigFig a = sigFig(1.234, 3);
    sigFig b = sigFig(2.481632, 5);
    sigFig c = sigFig_multiply(a, b);
    ASSERT_DOUBLE_EQ(3.05, c.value());
    ASSERT_EQ(3, c.prec());
}
TEST(sigFigMath, create_sigFig_value){
    sigFig test_fig = sigFig(1234567,4);
    ASSERT_EQ(1235000, test_fig.value());
    ASSERT_EQ(4, test_fig.prec());
}
TEST(sigFigMath, rounding){
    ASSERT_EQ(1230000,sigFig_round(1234567,3));
}
TEST(UniversalConstants, constant_values){
    ASSERT_DOUBLE_EQ(6.67259E-11, G.value());
    ASSERT_DOUBLE_EQ(149597870700., AU.value() );
}
TEST(DataFileTests, ICRF2_data){
    dataFile bodyDataFile = dataFile(data_file_loc,"ICRF2");
    EXPECT_TRUE(bodyDataFile.exists());
    EXPECT_EQ(11, bodyDataFile.id_vec.size());
    EXPECT_EQ(11, bodyDataFile.data_vec.size());
    EXPECT_EQ(7, bodyDataFile.data_vec[0].size());
    EXPECT_DOUBLE_EQ(132712440041.93940, bodyDataFile.data_vec[0][0]);
}
TEST(DataFileTests, empty_init){
    dataFile testDataFile = dataFile(" "," ");
    EXPECT_FALSE(testDataFile.exists());
}
TEST(CelestialBodyTests, known_Earth_values){
    dataFile bodyDataFile = dataFile("./../data/ICRF2_Data","ICRF2");
    CelestialBody test_body = CelestialBody(bodyDataFile, 3);
    ASSERT_DOUBLE_EQ(3.98600435436E+14, test_body.mu());
}
TEST(CelestialBodyTests, body_is_initialized_empty){
    CelestialBody test_body = CelestialBody();
    ASSERT_FALSE( test_body.init_successful() );
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
